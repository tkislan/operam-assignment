import fs from 'fs';
import fetch from 'node-fetch';
import { parseString as xml2Json } from 'xml2js';

import { createSqliteDatabase, sqliteRun } from '../src/utils/sqlite-helpers';

function parseXml(xmlData) {
  return new Promise((resolve, reject) => {
    xml2Json(xmlData, (error, result) => {
      if (error) return reject(error);
      return resolve(result);
    });
  });
}

function parseData(synset, prefix = '') {
  let output = [];

  const { words: name, subtree_size: size } = synset.$;

  console.log(`Adding name: ${prefix}${name}, size: ${parseInt(size, 10)}`);
  output.push({ name: `${prefix}${name}`, size: parseInt(size, 10) });

  if (synset.synset && synset.synset.length > 0) {
    console.log(synset.synset[0]);
    synset.synset.forEach((s) => { output = [...output, ...parseData(s, `${prefix}${name} > `)]; });
  }

  return output;
}

function insertImagenetData(db, data) {
  return new Promise((resolve, reject) => {
    db.serialize(() => {
      db.run('begin transaction');

      db.run('delete from imagenet');

      const stmt = db.prepare('insert into imagenet values (?, ?);');

      data.forEach((r) => stmt.run(r.name, r.size));

      stmt.finalize();

      db.run('commit', (error) => (error ? reject(error) : resolve()));
    });
  });
}

const f = async () => {
  const db = await createSqliteDatabase();

  const cacheFile = '.xmldata.cache';

  let xmlData;

  if (fs.existsSync(cacheFile)) {
    console.log('Using cache file:', cacheFile);
    xmlData = fs.readFileSync(cacheFile);
  } else {
    console.log('Downloading data');
    const resp = await fetch('http://imagenet.stanford.edu/python/tree.py/SubtreeXML?rootid=82127&targetid=637');
    xmlData = await resp.text();
    fs.writeFileSync(cacheFile, xmlData);
  }

  // console.log(xmlData);

  const data = await parseXml(xmlData);

  console.log(data);

  const list = parseData(data.synset);

  console.log(list);
  console.log('list length:', list.length);

  await insertImagenetData(db, list);

  db.close();
};

f().then(() => {
  console.log('Done');
}, (error) => {
  console.log(error);
  process.exit(1);
});
