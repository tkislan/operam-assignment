FROM node:8.2.1-alpine

ENV HOME=/home/node

COPY . $HOME/app

WORKDIR $HOME/app

RUN apk update && \
  apk add --no-cache \
    ttf-ubuntu-font-family \
    sqlite \
    bash && \
  apk add --no-cache --virtual .build-deps \
    git \
    autoconf \
    automake \
    libtool \
    nasm \
    binutils-gold \
    g++ \
    gcc \
    libgcc \
    make \
    python && \
  npm install && \
  npm run build && \
  npm prune --production && \
  rm -r ./tests && \
  chown -R node:node $HOME/* $HOME/.config && \
  apk del .build-deps

ENV NODE_ENV=production

USER node

CMD ["npm", "start"]
