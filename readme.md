## Docker: docker-compose
```docker-compose up -d```
<br/>
```docker exec -it operamassignment_app_1 bash```

## Install and run
```npm install```
<br/>
```npm run start-dev```

To load data into database:
<br/>
```npm run download-data```

## Overview
Data is downloaded through node.js script, and loaded into sqlite database
<br/>
This is done by ```./scripts/download_data.js``` file

Loading data from database, and converting it into tree structure can be found in file ```./src/server/api/imagenet.js```
<br/>
Converting from list to tree is done in O(n) (if I don't consider the database sorting)
<br/>
Database querying with sorting should be O(n*logn)

All the gui part can be found in ```./src/gui```, most relevant being ```./src/gui/App.js```