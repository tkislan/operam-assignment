import { Database as SqliteDatabase } from 'sqlite3';

export function createSqliteDatabase() {
  return new Promise((resolve, reject) => {
    const db = new SqliteDatabase('./imagenet.db');
    db.on('error', (error) => reject(error));
    db.on('open', async () => {
      try {
        await sqliteRun(db, 'CREATE TABLE IF NOT EXISTS imagenet (name TEXT, size INTEGER);');
        resolve(db);
      } catch (error) {
        reject(error);
      }
    });
  });
}

export function sqliteRun(db, ...args) {
  return new Promise((resolve, reject) => {
    db.run(...args, (error) => (error ? reject(error) : resolve()));
  });
}

export function sqliteAll(db, ...args) {
  return new Promise((resolve, reject) => {
    db.all(...args, (error, rows) => (error ? reject(error) : resolve(rows)));
  });
}
