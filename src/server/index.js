import { createServer } from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import { Server as WsServer } from 'ws';

import { createSqliteDatabase } from '../utils/sqlite-helpers';

import api from './api';

const app = express();

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

createSqliteDatabase().then((db) => {
  app.use('/api', api({ db }));

  if (!process.env.PORT) throw new Error('Environment variable PORT not defined');
  const PORT = process.env.PORT;

  const server = createServer();

  server.on('request', app);

  const wss = new WsServer({ server, clientTracking: true, path: '/ws' });
  wss.on('connection', (ws) => {
    console.log('wss connection');

    ws.on('message', (msg) => {
      console.log('ws message:', msg);
    });
  });

  server.listen(PORT, (error) => {
    if (error) {
      console.error(error);
      return;
    }

    console.log(`Listening at http://*:${PORT}`);
  });

  const signalHandler = () => {
    console.log('Stopping server');
    server.close(() => {
      process.exit(0);
    });
  };

  process.on('SIGTERM', signalHandler);
  process.on('SIGINT', signalHandler);
}, (error) => {
  console.log(error);
  process.exit(1);
});
