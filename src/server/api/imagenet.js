import { Router } from 'express';

import { sqliteAll } from '../../utils/sqlite-helpers';

function getParentNode(nodes, prefix) {
  const parent = [];

  if (prefix.length > nodes.length) throw new Error('Invalid data');

  for (let i = 0; i < prefix.length; i += 1) {
    if (prefix[i] === nodes[i].name) {
      parent.push(nodes[i]);
    } else {
      throw new Error('Invalid data');
    }
  }

  return parent;
}

export default function({ db }) {
  const router = Router({ mergeParams: true });

  router.get('/data', async (req, res) => {
    try {
      const data = await sqliteAll(db, 'SELECT name, size FROM imagenet ORDER BY name');

      if (data.length === 0) return res.sendStatus(201);

      const [first, ...rest] = data;
      const rootNode = {
        name: first.name,
        size: first.size,
        children: [],
      };

      let currentNode = [rootNode];

      rest.forEach((r) => {
        const namePrefixArray = r.name.split(' > ');
        const name = namePrefixArray.pop();

        const node = { name, size: r.size, children: [] };

        const parentTree = getParentNode(currentNode, namePrefixArray);
        const parent = parentTree.pop();

        parent.children.push(node);

        currentNode = [...parentTree, parent, node];
      });

      return res.json({ data: rootNode });
    } catch (error) {
      console.log(error);
      return res.status(400).json({ error: error.message });
    }
  });

  return router;
}
