import { Router } from 'express';

import imagenet from './imagenet';

export default function(meta) {
  const router = Router({ mergeParams: true });

  router.use('/imagenet', imagenet(meta));

  router.use('*', (req, res) => {
    res.status(404).json({ error: 'API call doesn\'t exist' });
  });

  return router;
}
