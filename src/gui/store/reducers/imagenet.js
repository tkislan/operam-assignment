import { GET_IMAGENET_DATA } from '../actions/imagenet';

const initialState = {
  pending: false,
  data: null,
  error: null,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case `${GET_IMAGENET_DATA}_PENDING`:
      return { ...state, pending: true };
    case `${GET_IMAGENET_DATA}_OK`:
      return { ...state, pending: false, data: action.payload.data, error: null };
    case `${GET_IMAGENET_DATA}_ERROR`:
      return { ...state, pending: false, data: null, error: action.error };
    default:
      return state;
  }
}
