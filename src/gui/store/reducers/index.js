import { combineReducers } from 'redux';

import imagenet from './imagenet';

export default combineReducers({
  imagenet,
});
