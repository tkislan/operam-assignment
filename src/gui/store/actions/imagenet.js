export const GET_IMAGENET_DATA = 'GET_IMAGENET_DATA';

export function getImagenetData() {
  return { type: GET_IMAGENET_DATA };
}
