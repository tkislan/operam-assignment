let createStore;
if (process.env.NODE_ENV === 'production') {
  createStore = require('./configureStore.prod');
} else {
  createStore = require('./configureStore.dev');
}

export default createStore;
