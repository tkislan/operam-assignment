import { fork } from 'redux-saga/effects';

import imagenet from './imagenet';

export default function* root() {
  yield fork(imagenet);
}
