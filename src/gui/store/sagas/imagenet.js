import { put, call, fork, delay, takeEvery } from 'redux-saga/effects';

import { GET_IMAGENET_DATA } from '../actions/imagenet';

export function* getImagenetData() {
  try {
    yield put({ type: `${GET_IMAGENET_DATA}_PENDING` });

    // delay(3000);
    const resp = yield fetch('/api/imagenet/data');

    console.log(resp);

    switch (resp.status) {
      case 200: {
        const data = yield resp.json();

        yield put({ type: `${GET_IMAGENET_DATA}_OK`, payload: data });
        break;
      }
      case 201:
        yield put({ type: `${GET_IMAGENET_DATA}_ERROR`, error: 'No data found' });
        break;
      default:
        yield put({ type: `${GET_IMAGENET_DATA}_ERROR`, error: 'Failed to get data' });
        break;
    }
  } catch (error) {
    yield put({ type: `${GET_IMAGENET_DATA}_ERROR`, error: error.message });
  }
}

function* watchGetImagenetData() {
  yield takeEvery(GET_IMAGENET_DATA, getImagenetData);
}

export default function* () {
  yield fork(watchGetImagenetData);
}
