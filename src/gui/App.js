import React from 'react';

import CircularProgress from 'material-ui/CircularProgress';
import Paper from 'material-ui/Paper';
import MenuItem from 'material-ui/MenuItem';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';

import { rootStyle } from './App.scss';

console.log('rootStyle:', rootStyle);

class Node extends React.PureComponent {
  state = {
    expanded: false,
  };

  handleExpand = () => {
    console.log('expand/collapse');
    this.setState({ expanded: !this.state.expanded });
  };

  render() {
    const { node } = this.props;
    const { expanded } = this.state;

    const hasChildren = node.children && node.children.length > 0;

    return (
      <div className="node">
        <MenuItem
          primaryText={`${node.name} (${node.size})`}
          onTouchTap={hasChildren ? this.handleExpand : undefined}
          rightIcon={hasChildren ? (<NavigationExpandMoreIcon className={`node-icon ${expanded ? 'rotate' : ''}`} />) : undefined}
        />
        {expanded && (
          <div className="node-subitems">
            {node.children.map((c, i) => (
              <Node key={i} node={c} />
            ))}
          </div>
        )}
      </div>
    );
  }
}

export default class App extends React.PureComponent {
  constructor(props) {
    super(props);

    props.getImagenetData();
  }

  render() {
    const { loading, data, error } = this.props;
    // const loading = true;

    console.log('render:', this.props);

    return (
      <div className={rootStyle}>
        {loading && (
          <div className="loading">
            <CircularProgress className="indicator" size={150} thickness={7} />
          </div>
        )}
        {!loading && data && (
          <Paper className="data">
            <Node node={data} />
          </Paper>
        )}
        {error && (
          <div className="error"><h3>{error}</h3></div>
        )}
      </div>
    );
  }
}

