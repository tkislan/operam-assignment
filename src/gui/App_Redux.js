import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import App from './App';

import { getImagenetData } from './store/actions/imagenet';

function mapStateToProps(state) {
  return {
    loading: state.imagenet.pending,
    data: state.imagenet.data,
    error: state.imagenet.error,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ getImagenetData }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
