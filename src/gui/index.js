import React from 'react';
import { render } from 'react-dom';

import injectTapEventPlugin from 'react-tap-event-plugin';

import { addLocaleData } from 'react-intl';
import enLocaleData from 'react-intl/locale-data/en';

import './index.scss';

import AppContainer from './AppContainer';

if (!window.tapEventInjected) {
  injectTapEventPlugin();
  window.tapEventInjected = true;
}

if (!window.localeDataAdded) {
  addLocaleData(enLocaleData);
  window.localeDataAdded = true;
}

render(<AppContainer />, document.getElementById('app'));

if (module.hot) {
  module.hot.accept((error) => {
    if (error) {
      console.error('Cannot apply hot update:', error);
    }
  });
}
